import JSSip from "@krivega/jssip";

const serverUrl = "172.20.20.196";
const conference = 1000;

const configuration = {
  displayName: "CrazyUmka",
  userAgent: "Chrome",
  uri: `sip:10000@${serverUrl}`,
  session_timers: false,
  sockets: [new JSSip.WebSocketInterface(`wss://${serverUrl}/webrtc/wss/`)],
};

window.mediaStream = null;
window.ua = new JSSip.UA(configuration);
window.ua.on("connected", _createCaller);

function _createCaller() {
  const session = window.ua.call(`sip:${conference}@${serverUrl}`, {
    mediaStream,
    eventHandlers: {
      failed: (e) => console.log(e),
      ended: (e) => console.log(e),
      peerconnection: (o) => {
        console.log(o);
        const { peerconnection } = o;
        peerconnection.ontrack = (pc) => {
          console.log(pc);
          const { track } = pc;
          if (track.kind === "video" && track.readyState === "live") {
            const remoteStream = session.connection.getRemoteStreams();
            console.log(remoteStream);
          }
        };
      },
    },
  });
}

async function main() {
  mediaStream = await navigator.mediaDevices.getUserMedia({
    audio: true,
    video: true,
  });
  const el = document.querySelector("#self-video");
  el.srcObject = window.mediaStream;
  ua.start();
}

main();
